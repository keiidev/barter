package dev.keii.barter.commands;

import dev.keii.barter.Barter;
import dev.keii.barter.Database;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;


public class CommandBarter implements CommandExecutor {
    private void sendInfo(CommandSender sender)
    {
        sender.sendMessage(Component.text("Keii's Barter").color(NamedTextColor.GOLD));
        sender.sendMessage(Component.text("Version: " + Barter.getInstance().getDescription().getVersion()).color(NamedTextColor.YELLOW));
    }

    private void sendHelp(CommandSender sender)
    {
        sender.sendMessage(
                Component.text("")
                        .append(Component.text("/barter").color(NamedTextColor.GOLD))
                        .appendNewline()
                        .append(Component.text("    info - Get info about the plugin").color(NamedTextColor.YELLOW))
                        .appendNewline()
                        .append(Component.text("    save - Save the data").color(NamedTextColor.YELLOW))
                        .appendNewline()
                        .append(Component.text("    load - Load the data").color(NamedTextColor.YELLOW))
                        .appendNewline()
                        .append(Component.text("    help - Get help for barter").color(NamedTextColor.YELLOW))
        );
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(args.length < 1)
        {
            sendHelp(sender);
            return true;
        }

        switch(args[0])
        {
            case "info":
                sendInfo(sender);
                break;
            case "save":
                Database.save(sender);
                break;
            case "load":
                Database.load(sender);
                break;
            case "help":
            default:
                sendHelp(sender);
        }

        return true;
    }
}
