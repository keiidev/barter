package dev.keii.barter;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

import javax.annotation.Nullable;
import java.io.File;
import java.sql.*;

public class Database {
    public static void initializeDatabase() {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String sql = """
                    CREATE TABLE IF NOT EXISTS shops (
                       chest_x integer NOT NULL,
                       chest_y integer NOT NULL,
                       chest_z integer NOT NULL,
                       chest_world text NOT NULL,
                       sign_x integer NOT NULL,
                       sign_y integer NOT NULL,
                       sign_z integer NOT NULL,
                       sign_world text NOT NULL,
                       selling_item text NOT NULL,
                       selling_amount integer NOT NULL,
                       payment_item text NOT NULL,
                       payment_amount integer NOT NULL,
                       owner_uuid text NOT NULL
                    );""";
            statement.execute(sql);

            sql = """
                    CREATE TABLE IF NOT EXISTS player_chests (
                       x integer NOT NULL,
                       y integer NOT NULL,
                       z integer NOT NULL,
                       world text NOT NULL,
                       player_uuid text NOT NULL
                    );""";
            statement.execute(sql);

            statement.close();
        } catch (SQLException e)
        {
            Barter.sendMessageToStaff(Component.text("Error initializing database: " + e.getMessage()));
        }
    }

    private static  Connection getConnection() throws SQLException {
        File f = new File("./plugins/Barter");
        f.mkdir();

        String url = "jdbc:sqlite:./plugins/Barter/database.db";
        // create a connection to the database
        return DriverManager.getConnection(url);
    }

    public static void save(@Nullable  CommandSender sender) {
        if (sender != null) {
            if (!sender.hasPermission("keii.barter.save")) {
                sender.sendMessage(Component.text("No permission").color(NamedTextColor.RED));
                return;
            }
        }

        Barter.sendMessageToStaff(Component.text("Saving Barter!").color(NamedTextColor.YELLOW));

        Connection connection;

        initializeDatabase();

        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();

            statement.execute("DELETE FROM shops;");
            statement.execute("DELETE FROM player_chests;");

            for(Shop shop : Barter.shops) {
                statement.execute("INSERT INTO shops(chest_x, chest_y, chest_z, chest_world, sign_x, sign_y, sign_z, sign_world, selling_item, selling_amount, payment_item, payment_amount, owner_uuid) VALUES(" + shop.chestLocation.getX() + ", " + shop.chestLocation.getY() + ", " + shop.chestLocation.getZ() + ", '" + shop.chestLocation.getWorld().getName() + "', " + shop.signLocation.getX() + ", " + shop.signLocation.getY() + ", " + shop.signLocation.getZ() + ", '" + shop.signLocation.getWorld().getName() + "', '" + shop.sellingItem.getKey().getKey() + "', " + shop.sellingAmount + ", '" + shop.paymentItem.getKey().getKey() + "', " + shop.paymentAmount + ", '" + shop.ownerUUID + "')");
            }

            for(Location location : Barter.playerChests.keySet())
            {
                statement.execute("INSERT INTO Player_chests(x, y, z, world, player_uuid) VALUES(" + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", '" + location.getWorld().getName() + "', '" + Barter.playerChests.get(location) + "')");
            }

            connection.commit();

            statement.close();
            connection.close();
        } catch(SQLException e)
        {
            Barter.sendMessageToStaff(Component.text("Failed saving Barter! " + e.getMessage()).color(NamedTextColor.RED));
            return;
        }

        Barter.sendMessageToStaff(Component.text("Finished saving Barter!").color(NamedTextColor.GREEN));
    }


    public static void load(@Nullable CommandSender sender)
    {
        if(sender != null) {
            if (!sender.hasPermission("keii.barter.load")) {
                sender.sendMessage(Component.text("No permission").color(NamedTextColor.RED));
                return;
            }
        }

        Barter.sendMessageToStaff(Component.text("Loading Barter!").color(NamedTextColor.YELLOW));

        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();

            ResultSet shops = statement.executeQuery("SELECT * FROM shops");

            Barter.shops.clear();
            while(shops.next()) {
                int chestX = shops.getInt("chest_x");
                int chestY = shops.getInt("chest_y");
                int chestZ = shops.getInt("chest_z");
                String chestWorld = shops.getString("chest_world");

                int signX = shops.getInt("sign_x");
                int signY = shops.getInt("sign_y");
                int signZ = shops.getInt("sign_z");
                String signWorld = shops.getString("sign_world");

                String sellingItem = shops.getString("selling_item");
                int sellingAmount = shops.getInt("selling_amount");

                String paymentItem = shops.getString("payment_item");
                int paymentAmount = shops.getInt("payment_amount");

                String ownerUUID = shops.getString("owner_uuid");

                Barter.shops.add(new Shop(new Location(
                            Bukkit.getWorld(chestWorld),
                            chestX, chestY, chestZ
                        ),
                        new Location(
                                Bukkit.getWorld(signWorld),
                                signX, signY, signZ
                        ),
                        Material.valueOf(sellingItem),
                        sellingAmount,
                        Material.valueOf(paymentItem),
                        paymentAmount,
                        ownerUUID
                    ));
            }
            shops.close();

            ResultSet playerChests = statement.executeQuery("SELECT * FROM player_chests");

            Barter.playerChests.clear();
            while(playerChests.next())
            {
                int x = playerChests.getInt("x");
                int y = playerChests.getInt("y");
                int z = playerChests.getInt("z");
                String world = playerChests.getString("world");
                String playerUUID = playerChests.getString("player_uuid");

                Barter.playerChests.put(
                        new Location(
                                Bukkit.getWorld(world),
                                x,
                                y,
                                z
                        ),
                        playerUUID
                );
            }
            playerChests.close();

            connection.commit();
            statement.close();
            connection.close();
        } catch(SQLException e)
        {
            if(connection != null)
            {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    Barter.sendMessageToStaff(Component.text("Failed Barter load rollback! " + ex.getMessage()).color(NamedTextColor.RED));
                }
            }

            Barter.sendMessageToStaff(Component.text("Failed loading Barter! " + e.getMessage()).color(NamedTextColor.RED));
        }

        Barter.sendMessageToStaff(Component.text("Finished loading Barter!").color(NamedTextColor.GREEN));
    }
}

