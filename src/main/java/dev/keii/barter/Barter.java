package dev.keii.barter;

import dev.keii.barter.commands.CommandBarter;
import dev.keii.barter.events.BlockBreak;
import dev.keii.barter.events.BlockPlace;
import dev.keii.barter.events.PlayerInteract;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public final class Barter extends JavaPlugin {

    public static List<Shop> shops = new ArrayList<>();
    public static HashMap<Location, String> playerChests = new HashMap<>();
    public static HashMap<Location, Integer> chestLocationCache = new HashMap<>();
    public static HashMap<Location, Integer> signLocationCache = new HashMap<>();

    private static final List<Material> signs = Arrays.asList(Material.ACACIA_WALL_SIGN, Material.SPRUCE_WALL_SIGN, Material.BAMBOO_WALL_SIGN, Material.BIRCH_WALL_SIGN, Material.CHERRY_WALL_SIGN, Material.CRIMSON_WALL_SIGN, Material.DARK_OAK_WALL_SIGN, Material.JUNGLE_WALL_SIGN, Material.MANGROVE_WALL_SIGN, Material.WARPED_WALL_SIGN, Material.OAK_WALL_SIGN);
    private static final List<Material> chests = List.of(Material.CHEST);

    public static boolean isSign(Block block)
    {
        return signs.contains(block.getType());
    }
    public static boolean isChest(Block block)
    {
        return chests.contains(block.getType());
    }

    public static void sendMessageToStaff(Component text)
    {
        Bukkit.getConsoleSender().sendMessage(text);
        for(Player player : Bukkit.getOnlinePlayers())
        {
            if(player.hasPermission("keii.barter.staff"))
            {
                player.sendMessage(text);
            }
        }
    }
    private static Barter instance = null;

    @Override
    public void onEnable() {
        instance = this;

        Database.initializeDatabase();
        Database.load(null);

        registerEvents();
        registerCommands();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void registerEvents() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new PlayerInteract(), this);
    }

    private void registerCommands() {
        this.getCommand("barter").setExecutor(new CommandBarter());
    }

    public static Barter getInstance()
    {
        return instance;
    }
}
