package dev.keii.barter;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

public class Shop {
    public Location chestLocation;
    public Location signLocation;

    public Material sellingItem;
    public int sellingAmount;

    public Material paymentItem;
    public int paymentAmount;

    public String ownerUUID;

    public Shop(Location chestLocation, Location signLocation, Material sellingItem, int sellingAmount, Material paymentItem, int paymentAmount, String ownerUUID)
    {
        this.chestLocation = chestLocation;
        this.signLocation = signLocation;
        this.sellingItem = sellingItem;
        this.sellingAmount = sellingAmount;
        this.paymentItem = paymentItem;
        this.paymentAmount = paymentAmount;
        this.ownerUUID = ownerUUID;
    }
}
