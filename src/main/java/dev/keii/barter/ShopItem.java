package dev.keii.barter;

public class ShopItem {
    public String displayName;
    public String name;
    public int amount = 0;

    public ShopItem(String displayName, String name, int amount)
    {
        this.displayName = displayName;
        this.name = name;
        this.amount = amount;
    }

    public ShopItem(String displayName, String name)
    {
        this.displayName = displayName;
        this.name = name;
    }

}
