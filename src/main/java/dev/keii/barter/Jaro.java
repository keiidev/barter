package dev.keii.barter;

public class Jaro {

    public static double similarity(String s1, String s2) {
        if (s1 == null || s2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }

        int str1Len = s1.length();
        int str2Len = s2.length();

        if (str1Len == 0 && str2Len == 0) {
            return 1;
        }

        int matchDistance = Integer.max(str1Len, str2Len) / 2 - 1;

        boolean[] str1Matches = new boolean[str1Len];
        boolean[] str2Matches = new boolean[str2Len];

        int matches = 0;
        for (int i = 0; i < str1Len; i++) {
            int start = Integer.max(0, i - matchDistance);
            int end = Integer.min(i + matchDistance + 1, str2Len);

            for (int j = start; j < end; j++) {
                if (str2Matches[j]) continue;
                if (s1.charAt(i) != s2.charAt(j)) continue;
                str1Matches[i] = true;
                str2Matches[j] = true;
                matches++;
                break;
            }
        }
        if (matches == 0) return 0.0;

        int transpositions = 0;
        int k = 0;
        for (int i = 0; i < str1Len; i++) {
            if (!str1Matches[i]) continue;
            while (!str2Matches[k]) k++;
            if (s1.charAt(i) != s2.charAt(k)) transpositions++;
            k++;
        }

        double m = matches;
        return (m / str1Len + m / str2Len + (m - transpositions / 2.0) / m) / 3.0;
    }
}