package dev.keii.barter.events;

import dev.keii.barter.Barter;
import dev.keii.barter.Shop;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteract implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        if(event.getClickedBlock() == null)
        {
            return;
        }

        Block block = event.getClickedBlock();
        Shop shop = null;
        if(Barter.isSign(block))
        {
            Integer cacheIndex = Barter.signLocationCache.get(event.getClickedBlock().getLocation());
            if(cacheIndex == null)
            {
                int i = 0;
                for (Shop _shop : Barter.shops)
                {
                    if(_shop.signLocation == block.getLocation())
                    {
                        shop = _shop;
                        Barter.signLocationCache.put(shop.signLocation, i);
                        break;
                    }
                    i++;
                }
            } else {
                shop = Barter.shops.get(cacheIndex);
            }

            if(shop == null)
            {
                return;
            }

            event.setCancelled(true);

            Chest shopChest = (Chest) shop.chestLocation.getBlock().getState();
            Player player = event.getPlayer();

            if(!shopChest.getInventory().contains(shop.sellingItem, shop.sellingAmount))
            {
                player.sendActionBar(Component.text("Shop doesn't have the required items!").color(NamedTextColor.RED));
                player.playSound(player.getLocation(), Sound.BLOCK_STONE_HIT, 3.0f, 0.5f);
                return;
            }

            if(!player.getInventory().contains(shop.paymentItem, shop.paymentAmount))
            {
                player.sendActionBar(Component.text("You do not have enough items to pay!").color(NamedTextColor.RED));
                player.playSound(player.getLocation(), Sound.BLOCK_STONE_HIT, 3.0f, 0.5f);
                return;
            }

            ItemStack sellingItem = new ItemStack(shop.sellingItem, shop.sellingAmount);
            shopChest.getInventory().remove(sellingItem);
            player.getInventory().addItem(sellingItem);

            ItemStack paymentItem = new ItemStack(shop.paymentItem, shop.paymentAmount);
            shopChest.getInventory().addItem(paymentItem);
            player.getInventory().remove(paymentItem);

            player.sendActionBar(Component.text("You have bought " + sellingItem.getAmount() + " " + sellingItem.displayName() +"!").color(NamedTextColor.GREEN));
            player.playSound(player.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 3.0f, 0.5f);
        } else if(Barter.isChest(block))
        {
            Integer cacheIndex = Barter.chestLocationCache.get(event.getClickedBlock().getLocation());
            if(cacheIndex == null)
            {
                int i = 0;
                for (Shop _shop : Barter.shops)
                {
                    if(_shop.chestLocation == block.getLocation())
                    {
                        shop = _shop;
                        Barter.chestLocationCache.put(shop.chestLocation, i);
                        break;
                    }
                    i++;
                }
            } else {
                shop = Barter.shops.get(cacheIndex);
            }

            if(shop == null)
            {
                return;
            }

            if(!shop.ownerUUID.equals(event.getPlayer().getUniqueId().toString()))
            {
                event.setCancelled(true);
                return;
            }
        }
    }
}
