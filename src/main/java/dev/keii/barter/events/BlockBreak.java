package dev.keii.barter.events;

import dev.keii.barter.Barter;
import dev.keii.barter.Shop;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {
    private boolean unregisterShop(Shop shop, Player player)
    {
        if(!shop.ownerUUID.equals(player.getUniqueId().toString()))
        {
            player.sendActionBar(Component.text("You are not authorized break this shop!").color(NamedTextColor.RED));
            return false;
        }

        Barter.signLocationCache.remove(shop.signLocation);
        Barter.chestLocationCache.remove(shop.chestLocation);
        Barter.shops.remove(shop);

        player.sendActionBar(Component.text("You broke your shop!").color(NamedTextColor.YELLOW));
        return true;
    }

    @EventHandler
    public void onPlayerBlockBreak(BlockBreakEvent event)
    {
        Block block = event.getBlock();
        Shop shop = null;

        if(Barter.isSign(block))
        {
            Integer shopIndex = Barter.signLocationCache.get(event.getBlock().getLocation());
            if(shopIndex == null)
            {
                shopIndex = 0;
                for (Shop _shop : Barter.shops)
                {
                    if(_shop.signLocation == block.getLocation())
                    {
                        shop = _shop;
                        Barter.signLocationCache.put(shop.signLocation, shopIndex);
                        break;
                    }
                    shopIndex++;
                }
            } else {
                shop = Barter.shops.get(shopIndex);
            }

            if(shop == null)
            {
                return;
            }

            Player player = event.getPlayer();

            if(!unregisterShop(shop, player))
            {
                event.setCancelled(true);
            }
        } else if(Barter.isChest(block))
        {
            Integer shopIndex = Barter.chestLocationCache.get(event.getBlock().getLocation());
            if(shopIndex == null)
            {
                shopIndex = 0;
                for (Shop _shop : Barter.shops)
                {
                    if(_shop.chestLocation == block.getLocation())
                    {
                        shop = _shop;
                        Barter.chestLocationCache.put(shop.chestLocation, shopIndex);
                        break;
                    }
                    shopIndex++;
                }
            } else {
                shop = Barter.shops.get(shopIndex);
            }

            if(shop == null)
            {
                return;
            }

            Player player = event.getPlayer();

            if(!unregisterShop(shop, player))
            {
                event.setCancelled(true);
            }
        }
    }
}
