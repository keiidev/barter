package dev.keii.barter.events;

import dev.keii.barter.*;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.block.sign.Side;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

/*
Buying
64 oak logs
For
2 diamonds
 */

public class BlockPlace implements Listener {
    public Block getAttachedBlockForWallSign(Block block) {
        if (Barter.isSign(block)) {
            WallSign wallSignData = (WallSign) block.getBlockData();
            BlockFace attachedFace = wallSignData.getFacing().getOppositeFace();
            return block.getRelative(attachedFace);
        }
        return null;
    }

    private ShopItem parseItem(Component line, Player player)
    {
        String[] itemLine = PlainTextComponentSerializer.plainText().serialize(line).split(" ");
        int itemCount = 0;
        try {
            itemCount = Integer.parseInt(itemLine[0]);
        } catch(NumberFormatException e)
        {
            player.sendMessage(Component.text("Failed parsing number: " + itemLine[0]).color(NamedTextColor.RED));
            return null;
        }
        StringBuilder itemNameBuilder = new StringBuilder();
        for(int i = 0; i < itemLine.length; i++)
        {
            if(i == 0)
            {
                continue;
            }

            itemNameBuilder.append(itemLine[i]);
            itemNameBuilder.append(" ");
        }
        String itemName = itemNameBuilder.toString();

        ShopItem closestItem = new ShopItem("", "");
        double closestPercentage = 0;
        for(ShopItem item : ShopItems.shopItems)
        {
            double percentage = Jaro.similarity(item.displayName, itemName);
            if(percentage > closestPercentage)
            {
                closestPercentage = percentage;
                closestItem = item;
            }
        }

        if(closestPercentage < 0.95)
        {
            player.sendMessage(Component.text("Couldn't find an item with the specified name! Closest found: " + closestItem.displayName).color(NamedTextColor.RED));
            return null;
        }

        closestItem.amount = itemCount;
        return closestItem;
    }

    @EventHandler
    public void onPlayerBlockPlace(BlockPlaceEvent event)
    {
        Bukkit.broadcastMessage("1");

        Block block = event.getBlockPlaced();
        if(Barter.isChest(block))
        {
            Bukkit.broadcastMessage("2");

            Barter.playerChests.put(block.getLocation(), event.getPlayer().getUniqueId().toString());
            return;
        } else if(!Barter.isSign(block))
        {
            Bukkit.broadcastMessage("3");
            return;
        }

        Bukkit.broadcastMessage("4");

        Sign sign = (Sign) block.getState();

        List<Component> lines = sign.getSide(Side.FRONT).lines();

        if(!lines.get(0).toString().toLowerCase().contains("buying")) {
            return;
        }

        Bukkit.broadcastMessage("5");

        ShopItem sellItem = parseItem(lines.get(1), event.getPlayer());

        String itemLine3 = PlainTextComponentSerializer.plainText().serialize(lines.get(2)).toLowerCase();
        if(!itemLine3.equals("for"))
        {
            event.getPlayer().sendMessage(Component.text("Expected 'For' on line 3 found " + itemLine3).color(NamedTextColor.RED));
            return;
        }

        Bukkit.broadcastMessage("6");

        ShopItem buyItem = parseItem(lines.get(3), event.getPlayer());

        assert sellItem != null;
        assert buyItem != null;

        Material itemThatIsSold = Material.valueOf(sellItem.name);
        Material PaymentItem = Material.valueOf(buyItem.name);

        Bukkit.broadcastMessage("7");

        String chestUUID = Barter.playerChests.get(getAttachedBlockForWallSign(block).getLocation());

        if(chestUUID == null)
        {
            Bukkit.broadcastMessage("8");
            event.getPlayer().sendActionBar(Component.text("Can't create shop on chest that isn't yours. Try to replace the chest!").color(NamedTextColor.RED));
            return;
        } else if(!chestUUID.equals(event.getPlayer().getUniqueId().toString()))
        {
            Bukkit.broadcastMessage("9");
            event.getPlayer().sendActionBar(Component.text("Can't create shop on chest that isn't yours. Try to replace the chest!").color(NamedTextColor.RED));
            return;
        }

        Bukkit.broadcastMessage("10");
        Barter.shops.add(new Shop(getAttachedBlockForWallSign(block).getLocation(), block.getLocation(), itemThatIsSold, sellItem.amount, PaymentItem, buyItem.amount, event.getPlayer().getUniqueId().toString()));
        event.getPlayer().sendActionBar(Component.text("You created a shop!").color(NamedTextColor.GREEN));

        Bukkit.broadcastMessage("11");
    }
}
